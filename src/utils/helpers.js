export const getImageNFT = (number)=> `/images/${Number(number) % 10}.png`;

export const getNameNFT = (number) => `Mystery Box #${number}`;