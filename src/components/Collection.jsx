import React, { useEffect, useState } from "react";
import { Card, Image, Tooltip, Modal, Input, Spin, Button } from "antd";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { NFTMysteryBox, Exchange } from "contracts";
import { getImageNFT, getNameNFT } from "utils/helpers";
import Web3Context, { getContract } from "context/Web3Context";

const { Meta } = Card;

const styles = {
  NFTs: {
    display: "flex",
    flexWrap: "wrap",
    WebkitBoxPack: "start",
    justifyContent: "flex-start",
    margin: "0 auto",
    maxWidth: "1000px",
    gap: "10px",
  },
};

function Market() {
  const { currentAccount: walletAddress } = Web3Context();
  const [visible, setVisibility] = useState(false);
  const [nftToSend, setNftToSend] = useState(null);
  const [loading, setLoading] = useState(false);

  const { web3Contract: exchangeContract } = getContract(Exchange.abi, Exchange.address);
  const { web3Contract: nftContract } = getContract(NFTMysteryBox.abi, NFTMysteryBox.address);

  const [collects, setCollects] = useState([]);
  const [form, setForm] = useState({});
  const [isApprove, setIsApprove] = useState(false);

  useEffect(() => {
    if (walletAddress) {
      fetchCollectNFTs();
      checkApprove();
    }
  }, [walletAddress]);

  const checkApprove = () => {
    return nftContract.methods.isApprovedForAll(walletAddress, Exchange.address).call({
      from: walletAddress
    }).then(res => {
      setIsApprove(res);
      return res;
    }).catch(console.error);
  }

  const fetchCollectNFTs = () => {
    exchangeContract.methods.getListUserToken().call({
      from: walletAddress
    }).then(data => {
      let myCollects = [];
      data[1].forEach((item, idx) => {
        if (Number(item) && data?.[0]?.[idx]) {
          myCollects = [...myCollects, {
            tokenId: data?.[0]?.[idx],
            amount: item
          }];
        }
      });
      setCollects(myCollects);
    }).catch(e => {
      console.error(e);
      setCollects([]);
    });
  }


  async function sellNFT() {
    if (!isApprove) {
      const modal = Modal.error({
        title: "Error!",
        content: `You need approve to sell NFT`,
      });
      setTimeout(() => {
        modal.destroy();
      }, 5 * 1000);
      setVisibility(false);
      return;
    }
    setLoading(true);
    try {
      await exchangeContract.methods.sell(nftToSend.tokenId, form.amount, form.price).send({
        from: walletAddress,
      });
      setVisibility(false);
      setLoading(false);
      setForm({});
      fetchCollectNFTs();
      succList();
    } catch (e) {
      console.error(e);
      failList();
    }
  }


  async function approveAll() {
    setLoading(true);
    try {
      await nftContract.methods.setApprovalForAll(Exchange.address, true).send({
        from: walletAddress
      });
      const result = checkApprove();
      if (result) succApprove();
      else failApprove();
    } catch (e) {
      console.error(e);
      failApprove();
    }
    setLoading(false);
  }

  const handleSellClick = (nft) => {
    setNftToSend(nft);
    setVisibility(true);
  };

  function succList() {
    let secondsToGo = 5;
    const modal = Modal.success({
      title: "Success!",
      content: `Your NFT was listed on the marketplace`,
    });
    setTimeout(() => {
      modal.destroy();
    }, secondsToGo * 1000);
  }

  function succApprove() {
    let secondsToGo = 5;
    const modal = Modal.success({
      title: "Success!",
      content: `Approval is now set, you may sell your NFT`,
    });
    setTimeout(() => {
      modal.destroy();
    }, secondsToGo * 1000);
  }

  function failList() {
    let secondsToGo = 5;
    const modal = Modal.error({
      title: "Error!",
      content: `There was a problem listing your NFT`,
    });
    setTimeout(() => {
      modal.destroy();
    }, secondsToGo * 1000);
  }

  function failApprove() {
    let secondsToGo = 5;
    const modal = Modal.error({
      title: "Error!",
      content: `There was a problem with setting approval`,
    });
    setTimeout(() => {
      modal.destroy();
    }, secondsToGo * 1000);
  }

  const changeInput = (e) => {
    const { value, name } = e.target;
    const newForm = {
      ...form,
      [name]: value
    };
    setForm(newForm);
  }

  return (
    <>
      <div>
        <div style={styles.NFTs}>
          <Button
            onClick={approveAll}
            type="primary"
            style={{ marginBottom: '20px' }}
            disabled={isApprove}
          >
            Approve All
          </Button>
        </div>
        <div style={styles.NFTs}>
          {collects?.length &&
            collects.map((nft, index) => (
              <Card
                hoverable
                actions={[
                  <Tooltip title="List NFT for sale">
                    <ShoppingCartOutlined onClick={() => { handleSellClick(nft) }} />
                  </Tooltip>,
                ]}
                style={{ width: 240, border: "2px solid #e7eaf3" }}
                cover={
                  <Image
                    preview={false}
                    src={getImageNFT(nft?.tokenId)}
                    fallback="/images/no-image.png"
                    style={{ height: "240px" }}
                  />
                }
                key={index}
              >
                <Meta
                  title={getNameNFT(nft?.tokenId)}
                  description={`Amount: ${nft?.amount}`}
                />
              </Card>
            ))}
        </div>
      </div>
      <Modal
        title={`Sell NFT ${getNameNFT(nftToSend?.tokenId)}`}
        visible={visible}
        onCancel={() => setVisibility(false)}
        onOk={sellNFT}
        okText="Sell"
        footer={[
          <Button onClick={() => setVisibility(false)}>
            Cancel
          </Button>,
          <Button onClick={sellNFT} type="primary">
            Sell
          </Button>
        ]}
      >
        <Spin spinning={loading}>
          <img
            src={getImageNFT(nftToSend?.tokenId)}
            style={{
              width: "250px",
              margin: "0 auto 20px",
              borderRadius: "10px",
              marginBottom: "15px",
            }}
          />
          <Input
            autoFocus
            name="amount"
            style={{ marginBottom: '10px' }}
            value={form.amount ?? ''}
            placeholder="Amount"
            onChange={changeInput}
          />
          <Input
            name="price"
            value={form.price ?? ''}
            placeholder="Price Per Box"
            onChange={changeInput}
          />
        </Spin>
      </Modal>
    </>
  );
}

export default Market;
