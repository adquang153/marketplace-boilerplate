import Web3Context from "context/Web3Context";
import Blockies from "react-blockies";

/**
 * Shows a blockie image for the provided wallet address
 * @param {*} props
 * @returns <Blockies> JSX Elemenet
 */

function Blockie(props) {
  const { currentAccount } = Web3Context();
  if ((!props.address && !props.currentWallet) || !currentAccount) return null;

  return (
    <Blockies
      seed={props.currentWallet ? currentAccount.toLowerCase() : props.address.toLowerCase()}
      className="identicon"
      {...props}
    />
  );
}

export default Blockie;
