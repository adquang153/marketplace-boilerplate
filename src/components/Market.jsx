import React, { useState, useEffect } from "react";
import { Card, Image, Tooltip, Modal, Badge, Alert, Spin, Button } from "antd";
import {
  RightCircleOutlined,
} from "@ant-design/icons";
import { Exchange, ERC20 } from 'contracts';
import { STATUS_ORDER } from "utils/enum";
import { getImageNFT, getNameNFT } from 'utils/helpers';
import Web3Context, { getContract, web3 } from "context/Web3Context";

const { Meta } = Card;

function Collection() {

  const [visible, setVisibility] = useState(false);
  const [loading, setLoading] = useState(false);

  const { currentAccount: walletAddress } = Web3Context();

  const { web3Contract: exchangeContract } = getContract(Exchange.abi, Exchange.address);
  const { web3Contract: erc20Contract } = getContract(ERC20.abi, ERC20.address);

  const [collects, setCollects] = useState([]);
  const [nftCurrent, setNftCurrent] = useState(null);

  useEffect(() => {
    if (walletAddress) {
      fetchCollectNFTs();
    }
  }, [walletAddress]);

  const fetchCollectNFTs = async () => {
    try {
      const data = await exchangeContract.methods.getAllOrderByStatus(STATUS_ORDER.SELL).call();
      setCollects(data);
    } catch (e) {
      setCollects([]);
    }
  }

  const checkAllowance = () => erc20Contract.methods.allowance(walletAddress, Exchange.address).call();

  const purchase = async (orderId, price) => {
    setLoading(true);
    try {
      const allowance = await checkAllowance();
      if (allowance != price) {
        await erc20Contract.methods.approve(Exchange.address, price).send({
          from: walletAddress
        });
      }
      const result = await exchangeContract.methods.buy(orderId).send({
        from: walletAddress
      });
      web3.eth.getTransactionReceipt(result.transactionHash, function (error) {
        error ? failPurchase() : succPurchase();
      })
      fetchCollectNFTs();
      setVisibility(false);
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  }

  function succPurchase() {
    let secondsToGo = 5;
    const modal = Modal.success({
      title: "Success!",
      content: `You have purchased this NFT`,
    });
    setTimeout(() => {
      modal.destroy();
    }, secondsToGo * 1000);
  }

  function failPurchase() {
    let secondsToGo = 5;
    const modal = Modal.error({
      title: "Error!",
      content: `There was a problem when purchasing this NFT`,
    });
    setTimeout(() => {
      modal.destroy();
    }, secondsToGo * 1000);
  }

  const openModalNftCurrent = (item, orderId) => {
    setNftCurrent({ ...item, orderId });
    setVisibility(true);
  }

  const cancelSell = () => {
    Modal.confirm({
      title: `Do you want cancel sell ${getNameNFT(nftCurrent.tokenId)}`,
      cancelText: 'Cancel',
      okButtonProps: {
        danger: true,
        type: 'primary'
      },
      okText: 'Cancel Sell',
      onOk: async () => {
        try {
          setLoading(true);
          await exchangeContract.methods.cancelSell(nftCurrent.orderId).send({
            from: walletAddress
          });
          fetchCollectNFTs();
          const modal = Modal.success({
            title: 'Cancel sell success!',
          });
          setTimeout(() => { modal.destroy() }, 4000);
          setVisibility(false);
        } catch (e) {
          console.error(e);
        }
        setLoading(false);
      }
    });
  }

  return (
    <>
      <div style={{
        display: "flex",
        flexWrap: "wrap",
        WebkitBoxPack: "start",
        justifyContent: "flex-start",
        margin: "0 auto",
        maxWidth: "1000px",
        gap: "10px"
      }}>
        {collects.length ? collects.map(({ order, orderId }, idx) =>
          <Card
            hoverable
            actions={[
              <Tooltip title="View Collection">
                <RightCircleOutlined
                  onClick={() => openModalNftCurrent(order, orderId)}
                />
              </Tooltip>,
            ]}
            style={{ width: 240, border: "2px solid #e7eaf3" }}
            cover={
              <Image
                preview={false}
                src={getImageNFT(order.tokenId)}
                fallback={'/images/no-image.png'}
                style={{ height: "240px" }}
              />
            }
            key={idx}
          >
            <Meta title={`Order ${orderId} - NFT #${order?.tokenId}`} />
          </Card>) : <>
          <Alert
            message="Not found NFTs explore"
            type="error"
          />
          <div style={{ marginBottom: "10px" }}></div>
        </>}
      </div>
      <Modal
        title={`Buy ${getNameNFT(nftCurrent?.tokenId)}`}
        visible={visible}
        onCancel={() => !loading && setVisibility(false)}
        onOk={() => !loading && purchase(nftCurrent?.orderId, nftCurrent?.pricePerBox)}
        okText="Buy"
        footer={[
          <Button key="back" onClick={() => !loading && setVisibility(false)}>Cancel</Button>,
          String(nftCurrent?.owner).toLocaleLowerCase() === String(walletAddress).toLocaleLowerCase() ? <Button type="primary" danger onClick={cancelSell}>Cancel Sell</Button> : <></>,
          <Button type="primary" key="submit" onClick={() => !loading && purchase(nftCurrent?.orderId, nftCurrent?.pricePerBox)}>Buy</Button>,
        ]}
      >
        <Spin spinning={loading}>
          <div
            style={{
              width: "250px",
              margin: "auto",
            }}
          >
            <Badge.Ribbon
              color="green"
              text={`${nftCurrent?.pricePerBox} price per box`}
            >
              <img
                src={getImageNFT(nftCurrent?.tokenId)}

                style={{
                  width: "250px",
                  borderRadius: "10px",
                  marginBottom: "15px",
                }}
              />
              <p>Amount: {nftCurrent?.remainingAmount}</p>
            </Badge.Ribbon>
          </div>
        </Spin>
      </Modal>
    </>
  );
}

export default Collection;
