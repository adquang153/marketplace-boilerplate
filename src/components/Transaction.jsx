import React, { useEffect, useState } from "react";
import { Table, Tag, Space } from "antd";
import { ETHLogo } from "./Chains/Logos";
import { Exchange } from 'contracts';
import { getImageNFT, getNameNFT } from "utils/helpers";
import moment from 'moment';
import Web3Context, { getContract } from "context/Web3Context";

const styles = {
  table: {
    margin: "0 auto",
    width: "1000px",
  },
};

const columns = [
  {
    title: 'Name',
    dataIndex: 'tokenId',
    key: 'tokenId',
    render: (e) => getNameNFT(e)
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
  }, {
    title: 'Price',
    dataIndex: 'pricePerBox',
    key: 'pricePerBox',
    render: (e) => {
      return <Space size="middle">
        <ETHLogo />
        <span>{e}</span>
      </Space>
    }
  }, {
    title: 'Type',
    dataIndex: 'status',
    key: 'status',
    render: (status, a) => {
      const text = Number(status) ? 'BUY' : 'SELL';
      const color = Number(status) ? 'green' : 'volcano';
      return <Tag color={color}>{text}</Tag>;
    }
  }, {
    title: 'Image',
    dataIndex: 'tokenId',
    key: 'image',
    render: (tokenId) => {
      return <img src={getImageNFT(tokenId)} style={{ width: "40px", borderRadius: "4px" }} />;
    }
  }, {
    title: 'Date',
    dataIndex: 'time',
    key: 'time',
    render: (time) => {
      return <p>{moment(Number(`${time}000`)).format('YYYY-MM-DD HH:mm:ss')}</p>;
    }
  }
]

function Transaction() {
  const { currentAccount: walletAddress } = Web3Context();
  const { web3Contract: exchangeContract } = getContract(Exchange.abi, Exchange.address);
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    if (walletAddress) {
      fetchTransactions();
    }
  }, [walletAddress]);

  const fetchTransactions = () => {
    exchangeContract.methods.getListUserToken().call({
      from: walletAddress
    }).then(data => {
      setTransactions([...data?.[2] ?? []].reverse());
    }).catch(err => {
      console.error(err);
      setTransactions([]);
    });
  }

  return <>
    <div style={styles.table}>
      <Table dataSource={transactions} columns={columns} />
    </div>
  </>;
}


export default Transaction;
