import { n4 } from "utils/formatters";
import { useEffect, useState } from "react";
import * as ethers from 'ethers';
import Web3Context, { web3 } from "context/Web3Context";
import { getNativeByChain } from "utils/networks";

function NativeBalance() {

  const { currentAccount, chainId } = Web3Context();
  const nativeName = getNativeByChain(chainId);
  const [balance, setBalance] = useState();

  useEffect(() => {
    if (currentAccount) {
      web3.eth.getBalance(currentAccount)
        .then(balance => setBalance(ethers.utils.formatEther(balance)))
        .catch(console.error);
    }
  }, [currentAccount, chainId]);



  return (
    <div style={{ textAlign: "center", whiteSpace: "nowrap" }}>{`${balance ? n4.format(balance) : 0} ${nativeName}`}</div>
  );
}

export default NativeBalance;
