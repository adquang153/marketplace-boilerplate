import erc20Abi from './abi/ERC20.json';
import exchangeAbi from './abi/Exchange.json';
import nftAbi from './abi/NFTMysteryBox.json';

export const ERC20 = {
    address: '0x6a5085E9c820Fda977461178c3cdc125D9fE76d5',
    abi: erc20Abi
};

export const Exchange = {
    address: '0x172934E3B8EA450303aa0d63C9BDf67f346bCfa4',
    abi: exchangeAbi
}

export const NFTMysteryBox = {
    address: '0x553e6e742c76Fe98Ed4Cf8AE90ae3b93b2F2b7B1',
    abi: nftAbi
}

export const options = {
    gasPrice: 20000000000,
    gas: 1000000,
}