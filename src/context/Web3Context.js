import React, { createContext, useContext, useEffect, useState } from 'react';
import { ethers } from 'ethers';
import Web3 from 'web3';
import { useHistory } from 'react-router';


export const InitWeb3Context = createContext();

export default function Web3Context() {
    return useContext(InitWeb3Context);
}

const { ethereum } = window;

let myWeb3 = null;
if (ethereum) {
    myWeb3 = new Web3(ethereum);
}

export const web3 = myWeb3;

export const getContract = (abi, address) => {
    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();

    return {
        ethereumContract: new ethers.Contract(address, abi, signer),
        web3Contract: new web3.eth.Contract(abi, address)
    };
}

const CHECK_CONNECT_METAMASK = 'isConnectedMetamask';

export const Web3ContextProvider = ({ children }) => {

    const [currentAccount, setCurrentAccount] = useState(null);
    const [chainId, setChainId] = useState();
    const history = useHistory();

    const checkWalletConnected = async () => {
        if (!ethereum) return alert("Please install metamask");

        const accounts = await ethereum.request({ method: 'eth_accounts' });
        if(accounts?.length){
            setCurrentAccount(accounts[0]);
        }
    }

    const connectWallet = () => {
        if (!ethereum) return alert("Please install metamask");

        return ethereum.request({
            method: 'wallet_requestPermissions',
            params: [{eth_accounts:{}}]
        }).then((account)=>{
            setCurrentAccount(account[0].caveats[0].value[0]);
            localStorage.setItem(CHECK_CONNECT_METAMASK, true);
        }).catch(err=>{
            console.error('requestPermissions error', err);
        });
    }

    const getChainId = () => {
        if (!ethereum) return;
        web3.eth.getChainId().then(res => {
            const newChainId = web3.utils.toHex(res);
            if (chainId && chainId !== newChainId) history.go(0);
            else setChainId(web3.utils.toHex(res));
        });
    }

    const disconnectWallet = () => {
        localStorage.clear();
        history.go(0);
    }

    const switchChain =(chainId) => {
        return ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId }],
        }).then(()=>history.go(0));
    }

    const isAccountConnected = () => !!localStorage.getItem(CHECK_CONNECT_METAMASK);

    useEffect(() => {
        if(isAccountConnected()){
            checkWalletConnected();
        }
        getChainId();
    }, []);    

    useEffect(() => {
        const listenAccountChange = ()=>{
            ethereum.on('accountsChanged', function (accounts) {
                history.go(0);
            });
        }
        listenAccountChange();
        return ()=>{
            listenAccountChange();
        }
    }, []);

    return <InitWeb3Context.Provider value={{ connectWallet, currentAccount, checkWalletConnected, chainId, disconnectWallet, switchChain }}>
        {children}
    </InitWeb3Context.Provider>
}
