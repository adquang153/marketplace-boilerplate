import './App.css';
import "antd/dist/antd.css";
import { Menu, Layout } from "antd";
import {
  useHistory,
  Switch,
  Route,
  NavLink,
  Redirect,
  useLocation
} from "react-router-dom";
import Market from 'components/Market';
import Collection from 'components/Collection';
import Transaction from 'components/Transaction';
import NativeBalance from 'components/NativeBalance';
import Web3Context from 'context/Web3Context';
import { useEffect } from 'react';
import Chains from 'components/Chains';
import Account from 'components/Account';

const { Header } = Layout;

const styles = {
  content: {
    display: "flex",
    gap: "14px",
    justifyContent: "center",
    fontFamily: "Roboto, sans-serif",
    color: "#041836",
    marginTop: "130px",
    padding: "10px",
  },
  header: {
    position: "fixed",
    zIndex: 1,
    width: "100%",
    background: "#fff",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontFamily: "Roboto, sans-serif",
    borderBottom: "2px solid rgba(0, 0, 0, 0.06)",
    padding: "0 10px",
    boxShadow: "0 1px 10px rgb(151 164 175 / 10%)",
  },
  headerRight: {
    display: "flex",
    gap: "20px",
    alignItems: "center",
    fontSize: "15px",
    fontWeight: "600",
  },
};

function App() {

  const { pathname } = useLocation();

  return (
    <Layout style={{ height: "100vh", overflow: "auto" }}>
      <Header style={styles.header}>
        <Menu
          theme="light"
          mode="horizontal"
          style={{
            display: "flex",
            fontSize: "17px",
            fontWeight: "500",
            marginLeft: "50px",
            width: "100%",
            justifyContent: 'center'
          }}
          defaultSelectedKeys={[pathname]}
          items={[
            {
              key: "/market",
              label: <NavLink to="/market">🛒 Explore Market</NavLink>,
            }, {
              key: "/collection",
              label: <NavLink to="/collection">🖼 Your Collection</NavLink>
            }, {
              key: "/transaction",
              label: <NavLink to="/transaction">📑 Your Transactions</NavLink>
            }
          ]}
        >
        </Menu>
        <div style={styles.headerRight}>
          <Chains />
          <NativeBalance />
          <Account />
        </div>
      </Header>
      <div style={styles.content}>
        <Switch>
          <Route path="/market">
            <Market />
          </Route>
          <Route path="/collection">
            <Collection />
          </Route>
          <Route path="/transaction">
            <Transaction />
          </Route>
        </Switch>
        {pathname === "/" && <Redirect to="market" />}
      </div>
    </Layout>
  );
}

export default App;
